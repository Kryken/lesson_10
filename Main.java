package com.hillel;

public class Main {
    public static void main(String[] args) {
        PrintLeapYear printLeapYear = new PrintLeapYear();
        printLeapYear.printLeapYear();
        System.out.println();
        PrintDateNow printDateNow = new PrintDateNow();
        printDateNow.printDate();
        System.out.println();
        PrintTuesday printTuesday = new PrintTuesday();
        printTuesday.printTuesday();
    }
}
