package com.hillel;
import java.time.*;

//Распечатать все високосные года за последние 10 лет
public class PrintLeapYear {
    public void printLeapYear() {
        LocalDate year = LocalDate.now();
        for (int i = year.getYear(); i > 2012; --i) {
            year = year.minusYears(1);
            if (year.isLeapYear()) {
                System.out.println(year);
            }
        }
    }
}
