package com.hillel;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.util.stream.IntStream;

//Создать программу что напечатает даты всех вторников текущего месяца
public class PrintTuesday {
    public void printTuesday() {
        int year = 2022;
        Month month = Month.APRIL;
        IntStream.rangeClosed(1, YearMonth.of(year, month).lengthOfMonth()).mapToObj(day->LocalDate.of(year, month, day))
                            .filter(date->date.getDayOfWeek() == DayOfWeek.TUESDAY).forEach(date->System.out.print(
                                    date.getYear() + "-" + date.getMonthValue() + "-" + date.getDayOfMonth() + "\t"));
    }
}
