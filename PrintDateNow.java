package com.hillel;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.time.LocalDate;

//Напечатать текущую дату в разных форматах
public class PrintDateNow {
    public void printDate() {
        LocalDate localDate = LocalDate.now();
        Locale us = new Locale("us", "US");
        String resultUS = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL).localizedBy(us).format(localDate);
        System.out.println(resultUS);
        Locale sst = new Locale("sst", "SST");
        String resultSST = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL).localizedBy(sst).format(localDate);
        System.out.println(resultSST);
        Locale ru = new Locale("ru", "RU");
        String resultRu = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL).localizedBy(ru).format(localDate);
        System.out.println(resultRu);
        Locale aut = new Locale("aut", "AUT");
        String resultAUT = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL).localizedBy(aut).format(localDate);
        System.out.println(resultAUT);
    }

}
